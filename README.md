## Raman geometry

This repository contains the solution to a geometry problem, permitting the use of a box for Raman spectroscopy. The solution is described within the Jupyter notebook `Raman_geometry.ipynb`, with an interactive version at [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/M_Arran%2Framan_geometry/HEAD?labpath=Raman_geometry.ipynb).

### Contents

* `Raman_geometry.svg`: An Inkscape Scalable Vector Graphics file containing a schematic of the system
* `Raman_geometry.png`: A Portable Network Graphics version of the above schematic
* `Raman_geometry.ipynb`: An interactive Jupyter notebook describing the solution, using the above schematic
* `Raman_geometry.html`: A static version of the above notebook
* `requirements.txt`: Text file specifying required Python module versions

### Use

Run the Jupyter notebook on Binder.